#!/bin/bash

# to be run from termux home folder after executing `termux-setup-storage` command
ping -c 1 rainbowdash.gnugen.ch
scp widmer@rainbowdash.gnugen.ch:music/*.opus storage/external-1/music/Downloaded
ssh widmer@rainbowdash.gnugen.ch 'rm -rf /home/widmer/music/*'

