#!/bin/bash

# you can store a default playlist ID here
# leave an empty string otherwise
PLAYLIST_ID="PLUEWW91P8xrF-MZZjuE4mGQs1u4bDOolI"

# set the path where the script is
YDL_PATH=$HOME/youtube-dl

# exit if bad arguments (to be improved)
if [ $# == 1 ]
then
    if [ "$1" == "--reset" ] # reset the download list and folder
    then
        echo "Reseting download list and music folder..."
        echo > "$YDL_PATH/downloaded"
        rm -rf "$YDL_PATH/music/*.opus"
        exit
    fi
    echo "Using playlist with ID $1"
    PLAYLIST_ID="$DEFAULT_PLAYLIST_ID"
else
    echo "No playlist ID passed as argument or wrong argument format."
    if [ "$PLAYLIST_ID" == "" ]
    then
        echo "No default playlist set. Exiting..."
        exit
    fi
    echo "Using default playlist with ID PLoX8lrU-_cbwDyHl0EjrZ_91ifoApfzMl"
fi

# store the list of videos to be downloaded
echo "Getting video IDs..."
youtube-dl -e --get-id "$PLAYLIST_ID" > $YDL_PATH/2bDL

# -q for quiet mode, remove for debbuging
# -i is for --ignore-errors: keep downlading if one video fails
# -x to extract audio
# --audio-quality 0 for the best quality (9 for the worse)
# -o output format for filename, see man page for more details
OPTIONS="-q -i -x --audio-format opus --audio-quality 0 -o %(title)s.%(ext)s"

# avoid already downloaded videos
# display the title (odd lines)
TITLE=1
while read -r line
do
    if [ "$TITLE" == 1 ]
    then
        TITLE="$line"
    else
        IS_PRESENT=$(cat < $YDL_PATH/downloaded | grep -c "$line")
        if [ "$IS_PRESENT" != "1" ]
        then
	    echo "Downloading $TITLE..."
            echo "$line" >> $YDL_PATH/downloaded
	    youtube-dl $OPTIONS $line
	    mv "$TITLE.opus" $YDL_PATH/music/
        fi
        TITLE=1 # next title
    fi
done < $YDL_PATH/2bDL

# move everything on RD (or any server) to fetch it later from phone
# assumes SSH connection without password available
#SERVER="rainbowdash.gnugen.ch"
#USERNAME="widmer"

#echo "Synching with server $SERVER"
#rsync -ruv $YDL_PATH/music $USERNAME@$SERVER:~/
#rm -rf $YDL_PATH/music/*

