set nu                          " display line number
syntax on                       " enable syntax coloring
set mouse=a                     " enable mouse use
set softtabstop=4 expandtab     " replace tab char with 4 spaces
set shiftwidth=4                " sets quto-indent to 4 spaces
set breakindent                 " indents wrapped line correctly

let g:Imap_UsePlaceHolders = 0  " disable latexsuite placeholders (<++>)

" miss-type
command W w
command Q q
command Wq wq
" builds latex file and refresh the ff tab that displays the result
command Bt !pdflatex % && sh ~/App/refresh-firefox-pdf.sh %

