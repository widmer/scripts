#!/bin/bash

# service list to start the LAMP server
declare -a SERVICES=("httpd" "mariadb" "php-fpm")

# variable to store user input. Default: show services statuses
COMMAND="show"

if [ $# -gt 1 ]
then
    echo "Wrong argument format."
    exit
elif [ $# = 0 ]
then
    echo "Default action: show services status..."
else
    COMMAND="$1"
fi

if [ "$COMMAND" = "show" ]
then
    for s in "${SERVICES[@]}"
    do
        systemctl status "$s" --no-pager
    done
elif [ "$COMMAND" = "start" ]
then
    for s in "${SERVICES[@]}"
    do
        sudo systemctl start "$s"
    done
elif [ "$COMMAND" = "stop" ]
then
    for s in "${SERVICES[@]}"
    do
        sudo systemctl stop "$s"
    done
elif [ "$COMMAND" = "restart" ]
then
    for s in "${SERVICES[@]}"
    do
        sudo systemctl restart "$s"
    done
else
    echo "Wrong argument."
    exit
fi
