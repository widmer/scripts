#!/usr/bin/python

import subprocess, os, sys, datetime, calendar, locale

locale.setlocale(locale.LC_ALL, "fr_CH.utf8")

file = "cal.tex"

today = datetime.datetime.now()
month = today.month
year = today.year

if len(sys.argv) == 2:
    month = int(sys.argv[1])

month_name = calendar.month_name[month].capitalize()

latex = r"""\documentclass[a4paper,12pt]{article}

\usepackage[a4paper,margin=1cm]{geometry}
\usepackage{tabularx}
\usepackage[table]{xcolor}

\setlength{\extrarowheight}{5pt}

\begin{document}
    \large
    \section*{\Huge """ + month_name + " " + str(year) + r"""}
    \vspace{10pt}
    \begin{center}
        \begin{tabularx}{\textwidth}{|l|X|X|}
            \hline
            \cellcolor{gray!20} \textbf{Jour} & \cellcolor{pink!60} \textbf{Tania} & \cellcolor{blue!25} \textbf{Ulysse} \\
            \hline"""

for i in range(1, calendar.monthrange(year, month)[1] + 1):
    day_number = str(i).zfill(2)
    day_name = calendar.day_name[calendar.weekday(year, month, i)]
    latex += """
            """ + day_number + " - " + day_name + r""" & \cellcolor{pink!20} & \cellcolor{blue!10} \\
            \hline"""

latex += r"""
        \end{tabularx}
    \end{center}
\end{document}
"""

with open(file, "w") as f:
    f.write(latex)

compiling = subprocess.run(['latexmk', '-pdf', file])
if compiling.returncode != 0:
    print("Exit-code not 0, check result!")
else:
    print("Cal generated successfully!")
cleaning = subprocess.run(['latexmk', '-c'])

os.remove("cal.tex")
