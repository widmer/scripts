#!/bin/bash

keystroke="F5"

# set to whatever's given as argument, defaults to firefox
BROWSER="firefox"
FILENAME="${1%.*}.pdf"

# find all visible browser windows
browser_windows="$(xdotool search --onlyvisible --name ^${FILENAME}.+${BROWSER}$)"
if [[ -z "$browser_windows" ]]
then
    firefox --new-window "$FILENAME" &
    exit
fi

# Send keystroke
for bw in $browser_windows; do
    xdotool key --window "$bw" "$keystroke"
done
