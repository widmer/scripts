# Home
Welcome to (a part of) my home folder!  
It contains various scripts I write and/or use, dotfiles and configuration files.  
The idea is to simplify synchronization between my computers and the deployment of my environment when I make a new install.

## ~
### Dotfiles
I use `zsh` as a shell. `.zshrc` contains my zhs config.
I usually use vim as an editor with the package group "vim-plugins" installed and a minimal .vimrc with some custom utilities.
#### GNUPG
The `.gnupg` directory and the `.zshrc` contain my configuration to enable ssh authentification with a GPG subkey. To complete it, simply add the keygrip of your subkey to `.gnupg/sshcontol` file and reload the `.zshrc` part: 
```bash
$ gpg -K --with-keygrip
$ echo <keygrip> >> ~/.gnupg/sshcontrol
$ source ~/.zshrc
```
If the `gpg-agent`didn't register your authentification sub-key correctly, ([bug](https://stackoverflow.com/questions/44250002/how-to-solve-sign-and-send-pubkey-signing-failed-agent-refused-operation), `sign_and_send_pubkey: signing failed: agent refused operation`), you can try to ru the following command
```bash
$ gpg-connect-agent updatestartuptty /bye
```
 
### ~/youtube-dl/
`youtube-dl.sh` gets the content of a personnal playlist that I fill with music I want on my phone. It searches for what hasn't been downladed yet and gets it. It then moves everything on a server, so I can fetch it from termux on my phone at any time. `phone-script.sh` is the script I use on my phone, in termux home folder.

### ~/App/
#### Minecraft
As I play Minecraft solo on different computers, I want my saves to synchronize with a remote server whenever I launch or close Minecraft.
`App/minecraft.sh` does just that. I modified `/usr/share/applications/minecraft-launcher.desktop` to use this script and make it seamless when I launch the minecraft launcher from my graphical environment.
#### refresh-firefox-pdf
Requires `xdotool`  
I use this small script to simplify the process of compiling and reloading firefox to display new changes when I write some LaTeX. An alias in my `.vimrc` compiles my file and calls this script, which reload the firefox window with the corresponding name.
#### cal-gen.py
Requires python and latex with latexmk.
I use this script to generate calendar pages for me and my gf whenever we forget to buy a proper calendar.
#### i3lock.sh
Two-line script to lock screen with a screenshot of my screen, without blur or anything.
